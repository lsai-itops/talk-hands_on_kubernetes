Examples are moved to [here](https://gitlab.lrz.de/lsai-itops/talk-hands_on_kubernetes/wikis/home)

All files are in this repo under /json

More about Kubernetes you find  [here](http://kubernetes.io/)
